// Get the maximum price of fruits on sale in the origin "Philippines"
db.fruits.aggregate([
    { $match: { origin: "Philippines", onSale: true } },
    { $group: {_id: "$name", maxPrice: {$max: "$price"}}},
    ]);